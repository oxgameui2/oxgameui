
import java.io.Serializable;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author sany
 */
public class Table implements Serializable{

    private static String[][] arr = new String[3][3];

    private Player playerX;
    private Player playerO;
    private Player turn;
    private int x = 0;
    private int o = 0;
    private String check = null;
    private boolean whoWin = false;
    private String winner = null;

    public Table(Player playerX, Player playerO) {
        this.playerX = playerX;
        this.playerO = playerO;
        this.turn = playerX;

        for (int i = 0; i < 3; i++) {
            arr[i][0] = "-";
            arr[i][1] = "-";
            arr[i][2] = "-";
        }
    }

    public void startBoard() {

        for (int i = 0; i < 3; i++) {
            arr[i][0] = "-";
            arr[i][1] = "-";
            arr[i][2] = "-";
        }

        System.out.println("Welcom to OX Game");
        System.out.println("  1 2 3");
        for (int i = 0; i < 3; i++) {
            System.out.println((i + 1) + " " + arr[i][0] + " " + arr[i][1] + " " + arr[i][2]);
        }

    }

    ////////////////////////////// board //////////////////////////////
    public void board() {
        System.out.println("  1 2 3");
        for (int i = 0; i < 3; i++) {
            System.out.println((i + 1) + " " + arr[i][0] + " " + arr[i][1] + " " + arr[i][2]);
        }
    }

    public String getRowCol(int row, int col) {
        return arr[row][col];
    }

    public boolean setRowCol(int row, int col) {
        if (whoWin == true) {
            return false;
        }
        if (arr[row - 1][col - 1].equals("-")) {
            arr[row - 1][col - 1] = turn.getName();
            return true;

        }
        return false;
    }

    public Player getTurn() {
        return turn;
    }

    public Player switchPlayer() {
        if (turn.equals(playerX)) {
            x = x + 1;
            turn = playerO;
        } else {
            o = o + 1;
            turn = playerX;
        }
        return turn;
    }

    public String winnerCheck() {

        for (int i = 1; i < 9; i++) {

            switch (i) {
                //  ---  //
                case 1:
                    check = arr[0][0] + arr[0][1] + arr[0][2];
                    break;
                case 2:
                    check = arr[1][0] + arr[1][1] + arr[1][2];
                    break;
                case 3:
                    check = arr[2][0] + arr[2][1] + arr[2][2];
                    break;

                //  |||  //   
                case 4:
                    check = arr[0][0] + arr[1][0] + arr[2][0];
                    break;
                case 5:
                    check = arr[0][1] + arr[1][1] + arr[2][1];
                    break;
                case 6:
                    check = arr[0][2] + arr[1][2] + arr[2][2];
                    break;

                //  /\  //    
                case 7:
                    check = arr[2][0] + arr[1][1] + arr[0][2];
                    break;
                case 8:
                    check = arr[0][0] + arr[1][1] + arr[2][2];
                    break;
            }
            if (check.equals("XXX")) {
                whoWin = true;
                return "X";
            } else if (check.equals("OOO")) {
                whoWin = true;
                return "O";
            }
        }
        //setStatWinLose();

        if (((!check.equals("XXX")) || (!check.equals("OOO"))) && x + o == 9) {
            whoWin = true;
            return "DRAW";
        } else {
            return null;
        }

    }

    public String printLastWhoWin() {
        if (!winnerCheck().equals("DRAW")) {
            setStatWinLose();
            return winnerCheck() + " Win....";
        } else {
            setStatWinLose();
            return "DRAW...";
        }
    }

    public boolean isWhoWin() {
        return whoWin;
    }

    public void setStatWinLose() {
        if (winnerCheck().equals("O")) {
            playerO.win();
            playerX.lose();
        } else if (winnerCheck().equals("X")){
            playerO.lose();
            playerX.win();
        }else{
            playerO.draw();
            playerX.draw();
        }
    }

}
